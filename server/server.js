require('./config/config');
const supported_currencies = require('./config/supportedCurrencies.json');
const mongoose = require('./db/mongoose');

const express = require('express');
const moment = require('moment');
const _ = require('lodash');

const app = express();
const port = process.env.PORT;
const {validateCurrencyPair, validateDate} = require('./utils/customValidators');
const {HistoricalRecord, LiveRecord} = require('./models/historicalRecord');


// TODO
// app.get('/rates/:pair/now', async (req, res) => {
//     try {
//         const pair = req.params.pair;
//         if ( !validateCurrencyPair(pair) ) {
//             return res.status(422).send({message: 'Currency pair is not valid'});
//         }
//
//
//     } catch (e) {
//         res.status(500).send({
//             error: {
//                 message: 'Internal Server Error'
//             }
//         });
//     }
// });

app.get('/rates/:pair/last_day', async (req, res) => {
    try {
        const pair = req.params.pair;
        if ( !validateCurrencyPair(pair) ) {
            return res.status(422).send({message: 'Currency pair is not valid'});
        }

        const to = moment().startOf('hour');
        const from = moment(to).subtract(24, 'hours');
        const isHourly = true;

        const search = await LiveRecord.findAllInRange(pair, from.format(), to.subtract(1, 'hours').format(), isHourly);
        res.status(search.status).send(search.body);

    } catch (e) {
        res.status(500).send({
            error: {
                message: 'Internal Server Error'
            }
        });
    }
});

app.get('/rates/:pair/last_week', async (req, res) => {
    try {
        const pair = req.params.pair;
        if ( !validateCurrencyPair(pair) ) {
            return res.status(422).send({message: 'Currency pair is not valid'});
        }

        const to = moment().startOf('hour');
        const from = moment(to).subtract(7, 'days');
        const isHourly = false;

        const search = await HistoricalRecord.findAllInRange(pair, from.format('YYYY-MM-DD'), to.subtract(1, 'days').format('YYYY-MM-DD'), isHourly);
        res.status(search.status).send(search.body);

    } catch (e) {
        res.status(500).send({
            error: {
                message: 'Internal Server Error'
            }
        });
    }
});

app.get('/rates/:pair/last_month', async (req, res) => {
    try {
        const pair = req.params.pair;
        if ( !validateCurrencyPair(pair) ) {
            return res.status(422).send({message: 'Currency pair is not valid'});
        }

        const to = moment().startOf('hour');
        const from = moment(to).subtract(1, 'months');
        const isHourly = false;

        const search = await HistoricalRecord.findAllInRange(pair, from.format('YYYY-MM-DD'), to.subtract(1, 'days').format('YYYY-MM-DD'), isHourly);
        res.status(search.status).send(search.body);

    } catch (e) {
        res.status(500).send({
            error: {
                message: 'Internal Server Error'
            }
        });
    }
});

// TODO: rewrite DB records from 12:00 UTC to (?) 00:00 UTC
// TODO: move scheduler for fetching (!) previous day data from 12:00 UTC to 00:00 UTC
// TODO: in requests, start day, week and month 1 time unit earlier then current
// TODO: consider counting rate for last time unit from live data

app.get('/rates/:pair/custom_range', async (req, res) => {
    try {
        const pair = req.params.pair;
        if ( !validateCurrencyPair(pair) ) {
            return res.status(422).send({message: 'Currency pair is not valid'});
        }

        const params = req.query;

        if ( params.hasOwnProperty('hourly') && ((params.hourly != 'true') && (params.hourly != 'false') )) {
            return res.status(422).send({message: 'Invalid hourly parameter'});
        }
        const isHourly = params.hourly == "true" ? true : false;

        if (params.hasOwnProperty('from') && params.hasOwnProperty('to')) {
            if (!validateDate(params.from) || !validateDate(params.to)) {
                return res.status(422).send({message: 'One or both dates are not valid'});
            }
            const from = moment.utc(params.from);
            const to = moment.utc(params.to);
            if (to.isBefore(from)) {
                return res.status(422).send({message: 'Range of dates is invalid'});
            }

            const search = isHourly ? await LiveRecord.findAllInRange(pair, from.format(), to.format(), isHourly)
                                    : await HistoricalRecord.findAllInRange(pair, from.format(), to.format(), isHourly);
            res.status(search.status).send(search.body);

        } else {
            res.status(400).send({
                error: {
                    message: 'Bad Request'
                }
            });
        }

    } catch (e) {
        res.status(500).send({
            error: {
                message: 'Internal Server Error'
            }
        });
    }

});

app.get('/currencies', (req, res) => {
    if (supported_currencies) {
        res.status(200).send({supported_currencies});
    } else {
        res.status(503).send({
            error: {
                message: 'Service Unavailable'
            }
        });
    }
});

app.listen(port, () => {
    console.log(`Server is up on port ${port}`);
});