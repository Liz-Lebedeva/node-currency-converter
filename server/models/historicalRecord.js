const mongoose = require('mongoose');
const moment = require('moment');
const _ = require('lodash');
// const validate = require('mongoose-validator');
// const validator = require('validator');

const {validateCurrencyPair, validateDate} = require('../utils/customValidators');

const formBody = (recordsArray) => {
    const pair = recordsArray[0].pair;
    return {
        [pair] : recordsArray.map( (record) => _.pick(record, ['date', 'rate']) )
    };
};

const HistoricalRecordSchema = new mongoose.Schema({
    pair: {
        type: String,
        required: [true, 'Pair cannot be empty'],
        validate: {
            validator: validateCurrencyPair,
            message: 'This pair is not supported'
        }
    },
    rate: {
        type: Number,
        required: [true, 'Rate cannot be empty']
    },
    date: {
        type: Date,
        required: [true, 'Date cannot be empty'],
        validate: {
            validator: validateDate,
            message: 'Invalid date (ISO 8601 required)'
        }
    }
});

// index
HistoricalRecordSchema.index({pair: 1, date: 1}, {unique: true});


HistoricalRecordSchema.post('save', function (error, res, next) {

    // DB has index that checks that compound key of 'pair' and 'date' fields is unique
    if (error.name === 'MongoError' && error.code === 11000) {
        next(new Error('This record already exists'));
    } else if (error) {
        next(new Error('Unable to save record'));
    } else {
        next();
    }
});


HistoricalRecordSchema.statics.findAllInRange = async function(pair, from, to, isHourly) {
    let Record = this;

    const fromUTC = isHourly ? moment.utc(from) : moment.utc(from).hours(12);
    const toUTC = isHourly ? moment.utc(to) : moment.utc(to).hours(12);
    const range = isHourly ? toUTC.diff(fromUTC, 'hours') : toUTC.diff(fromUTC, 'days');

    const records = await Record.find({
        pair,
        date: {
            $gte: fromUTC.format(),
            $lte: toUTC.format()
        }
    }).select({pair: 1, date: 1, rate: 1, _id: 0});

    if (!records) {
        return {
            status: 503,
            body: {
                error: {
                    message: 'Service Unavailable'
                }
            }
        };
    } else if (records.length === 0) {
        return {
            status: 404,
            body: {
                error: {
                    message: 'No records found'
                }
            }
        }
    } else if ( records.length > 0 && records.length < (range + 1) ) {
        return {
            status: 200,
            body: {
                data: formBody(records),
                error: {
                    message: 'Some records are missing. Partial results returned'
                }
            }
        };
    } else if ( records.length === (range + 1) ) {
        return {
            status: 200,
            body: {
                data: formBody(records)
            }
        };
    } else {
        return {
            status: 503,
            body: {
                error: {
                    message: 'Service Unavailable'
                }
            }
        };
    }
    // TODO: implement standardized errors above
};

const HistoricalRecord = mongoose.model('HistoricalRecord', HistoricalRecordSchema, 'historical-data');
const LiveRecord = mongoose.model('LiveRecord', HistoricalRecordSchema, 'live-data');

module.exports = {HistoricalRecord, LiveRecord};