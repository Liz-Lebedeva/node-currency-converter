require('../config/config');

const mongoose = require('mongoose');
mongoose.Promise = global.Promise;
mongoose.connect(process.env.MONGODB_URI, {autoIndex: true});

const {HistoricalRecord, LiveRecord} = require('./historicalRecord');

const moment = require('moment');
const expect = require('chai').expect;
const _ = require('lodash');

const filterInitialRecordsUTC = (recordsArray) => {
    return recordsArray.map((record) => {
        return {
            pair: record.pair,
            date: moment.utc(record.date).format(),
            rate: record.rate
        }
    });
};

const filterResults = (recordsArray) => {
    return recordsArray.map(record => _.pick(record, ['date', 'rate']));
};



describe('Daily Historical Records Model', function() {
    this.timeout(5000);
    this.slow(1000);

    /** Restore DB content before each test */

    let initialRecords;
    beforeEach( async function() {
        await HistoricalRecord.remove({});

        const records = require('../tests/seed/daily.json');
        let result = [];
        for (const record of records) {
            record.date = moment.utc(record.date).format();
            try {
                const dailyRecord = new HistoricalRecord(record);
                await dailyRecord.save();
                result.push(dailyRecord);
            } catch (e) {
                result.push(e.message);
            }
        }
        const dbrecords = await HistoricalRecord.find({});
        expect(filterInitialRecordsUTC(dbrecords)).to.deep.equal(filterInitialRecordsUTC(records));

        initialRecords = result;
    });


    /** Tests */

    it('should allow posting new records', async function() {
        // Define test data
        const newRecord = {
            pair: 'JPY_AUD',
            date: '2018-01-01T12:00:00Z',
            rate: 0.75
        };
        const initialUpdated = initialRecords.slice(0);
        initialUpdated.push(newRecord);

        await new HistoricalRecord(newRecord).save();

        const dbrecords = await HistoricalRecord.find({});
        expect(dbrecords.length).to.equal(initialUpdated.length);
        expect(filterInitialRecordsUTC(dbrecords)).to.deep.equal(filterInitialRecordsUTC(initialUpdated));
    });

    it('should not allow posting duplicate records (with the same Date and Pair)', async function() {
        // Define test data
        const newRecord = {
            pair: initialRecords[0].pair,
            date: initialRecords[0].date,
            rate: 2.5
        };

        try {
            await new HistoricalRecord(newRecord).save();
        } catch (e) {
            expect(e.message).to.include('record already exists'); // TODO: implement standardized errors
        }

        const dbrecords = await HistoricalRecord.find({});

        expect(dbrecords.length).to.equal(initialRecords.length);
        expect(filterInitialRecordsUTC(dbrecords)).to.deep.equal(filterInitialRecordsUTC(initialRecords));
    });



    it('findAllInRange should return one daily record if the defined range is 1 day', async function() {
        // Define test data
        const pair = 'USD_CAD';
        const date = moment.utc('2018-01-01').hour(12).format();

        const initialFiltered = initialRecords.filter(record => (
            record.pair === pair
            && moment.utc(record.date).isSame(moment.utc(date)))
        );

        const result = await HistoricalRecord.findAllInRange(pair, date, date, false);

        expect(result.status).to.equal(200);
        expect(result.body.error).to.be.undefined;// TODO: implement standardized errors
        expect(result.body.data[pair].length).to.equal(1);
        expect(result.body.data[pair]).to.deep.equal(filterResults(initialFiltered));
    });

    it('findAllInRange should return all daily records in defined range', async function() {
        // Define test data
        const pair = 'USD_CAD';
        const from = moment.utc('2018-01-01').hour(12).format();
        const to = moment.utc('2018-01-03').hour(12).format();

        const initialFiltered = initialRecords.filter(record => (
            record.pair === pair
            && moment.utc(record.date).isSameOrAfter(moment.utc(from))
            && moment.utc(record.date).isSameOrBefore(moment.utc(to)))
        );

        const result = await HistoricalRecord.findAllInRange(pair, from, to, false);

        expect(result.status).to.equal(200);
        expect(result.body.error).to.be.undefined;// TODO: implement standardized errors
        expect(result.body.data[pair].length).to.equal(3);
        expect(result.body.data[pair]).to.deep.equal(filterResults(initialFiltered));
    });

    it('findAllInRange should return all found daily records and \'partial\' status if some records are missing', async function() {
        // Define test data
        const pair = 'USD_CAD';
        const from = moment.utc('2018-01-03').hour(12).format();
        const to = moment.utc('2018-01-05').hour(12).format();

        const initialFiltered = initialRecords.filter(record => (
            record.pair === pair
            && moment.utc(record.date).isSameOrAfter(moment.utc(from))
            && moment.utc(record.date).isSameOrBefore(moment.utc(to)))
        );

        const result = await HistoricalRecord.findAllInRange(pair, from, to, false);

        expect(result.status).to.equal(200);
        expect(result.body.error.message).to.include('Partial results returned'); // TODO: implement standardized errors
        expect(result.body.data[pair].length).to.equal(2);
        expect(result.body.data[pair]).to.deep.equal(filterResults(initialFiltered));
    });

    it('findAllInRange should return no records and \'empty\' status if there is no records in defined period', async function() {
        // Define test data
        const pair = 'USD_CAD';
        const from = moment.utc('2018-02-01').hour(12).format();
        const to = moment.utc('2018-02-05').hour(12).format();

        const result = await HistoricalRecord.findAllInRange(pair, from, to, false);

        expect(result.status).to.equal(404);
        expect(result.body.error.message).to.include('No records found'); // TODO: implement standardized errors

        expect(result.body.data).to.be.undefined;
    });

});


describe('Hourly Live Records Model', function() {
    this.timeout(5000);
    this.slow(1000);

    /** Restore DB content before each test */

    let initialRecords;
    beforeEach( async function() {
        await LiveRecord.remove({});

        const records = require('../tests/seed/hourly.json');
        let result = [];
        for (const record of records) {
            record.date = moment.utc(record.date).format();
            try {
                const dailyRecord = new LiveRecord(record);
                await dailyRecord.save();
                result.push(dailyRecord);
            } catch (e) {
                result.push(e.message);
            }
        }

        const dbrecords = await LiveRecord.find({});
        expect(filterInitialRecordsUTC(dbrecords)).to.deep.equal(filterInitialRecordsUTC(records));

        initialRecords = result;
    });


    /** Tests */

    it('should allow posting new records', async function() {
        // Define test data
        const newRecord = {
            pair: 'JPY_AUD',
            date: '2018-01-01T10:00:00Z',
            rate: 50.05
        };
        const initialUpdated = initialRecords.slice(0);
        initialUpdated.push(newRecord);

        await new LiveRecord(newRecord).save();

        const dbrecords = await LiveRecord.find({});
        expect(dbrecords.length).to.equal(initialUpdated.length);
        expect(filterInitialRecordsUTC(dbrecords)).to.deep.equal(filterInitialRecordsUTC(initialUpdated));
    });

    it('should not allow posting duplicate records (with the same Hour and Pair)', async function() {
        // Define test data
        const newRecord = {
            pair: initialRecords[0].pair,
            date: initialRecords[0].date,
            rate: 10.55
        };

        try {
            await new LiveRecord(newRecord).save();
        } catch (e) {
            expect(e.message).to.include('record already exists'); // TODO: implement standardized errors
        }

        const dbrecords = await LiveRecord.find({});
        expect(dbrecords.length).to.equal(initialRecords.length);
        expect(filterInitialRecordsUTC(dbrecords)).to.deep.equal(filterInitialRecordsUTC(initialRecords));
    });

    it('findAllInRange should return all hourly records in defined range', async function() {
        // Define test data
        const pair = 'USD_CAD';
        const from = moment.utc('2018-01-01').startOf('day');
        const to = moment.utc('2018-01-01').hours(10);

        const initialFiltered = initialRecords.filter(record => (
            record.pair === pair
            && moment.utc(record.date).isSameOrAfter(moment.utc(from))
            && moment.utc(record.date).isSameOrBefore(moment.utc(to)))
        );

        const result = await LiveRecord.findAllInRange(pair, from, to, true);

        expect(result.status).to.equal(200);
        expect(result.body.error).to.be.undefined;// TODO: implement standardized errors
        expect(result.body.data[pair].length).to.equal(11);
        expect(result.body.data[pair]).to.deep.equal(filterResults(initialFiltered));
    });

    it('findAllInRange should return all found hourly records and \'partial\' status if some records are missing', async function() {
        // Define test data
        const pair = 'USD_CAD';
        const from = moment.utc('2018-01-01').hours(6);
        const to = moment.utc('2018-01-01').hours(12);

        const initialFiltered = initialRecords.filter(record => (
            record.pair === pair
            && moment.utc(record.date).isSameOrAfter(moment.utc(from))
            && moment.utc(record.date).isSameOrBefore(moment.utc(to)))
        );

        const result = await LiveRecord.findAllInRange(pair, from, to, true);

        expect(result.status).to.equal(200);
        expect(result.body.error.message).to.include('Partial results returned'); // TODO: implement standardized errors
        expect(result.body.data[pair].length).to.equal(6);
        expect(result.body.data[pair]).to.deep.equal(filterResults(initialFiltered));
    });

    it('findAllInRange should return no records and \'empty\' status if there is no records in defined period', async function() {
        // Define test data
        const pair = 'USD_CAD';
        const from = moment.utc('2018-05-01').startOf('day');
        const to = moment.utc('2018-05-01').hours(6);

        const result = await LiveRecord.findAllInRange(pair, from, to, true);

        expect(result.status).to.equal(404);
        expect(result.body.error.message).to.include('No records found'); // TODO: implement standardized errors

        expect(result.body.data).to.be.undefined;
    });

});
