/** This utility module contains functions that generate and return links for requests to currency converter API
 * to retrieve live and historical data.
 * I moved them to separate module to isolate these functions from the main code, so links could be easily fixed if necessary */

const historicalOneDayReq = (pairsArray, date) => {
    const pairsString = pairsArray.join(',');
    return `https://free.currencyconverterapi.com/api/v6/convert?q=${pairsString}&compact=ultra&date=${date}`;
};

const historicalRangeReq = (pairsArray, fromDate, toDate) => {
    const pairsString = pairsArray.join(',');
    return `https://free.currencyconverterapi.com/api/v6/convert?q=${pairsString}&compact=ultra&date=${fromDate}&endDate=${toDate}`;
};

const liveExRatesReq = (pairsArray) => {
    const pairsString = pairsArray.join(',');
    return `https://free.currencyconverterapi.com/api/v6/convert?q=${pairsString}&compact=ultra`;
};


module.exports = {historicalOneDayReq, historicalRangeReq, liveExRatesReq}