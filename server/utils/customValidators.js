const moment = require('moment');

const validateCurrencyPair = (pair) => {
    const supportedCur = require('./../config/supportedCurrencies.json');
    const supportedCurString = Object.values(supportedCur).join('|');
    const supportedCurRegex = new RegExp(`(${supportedCurString})_(${supportedCurString})`);

    if (supportedCurRegex.test(pair)) {
        const halves = pair.split('_');
        return halves[0] != halves[1];
    } else {
        return false;
    }
};

const validateDate = (date) => {
    const parsedDate = moment(date, moment.ISO_8601, true);
    return parsedDate.isValid();
};



module.exports = {validateCurrencyPair, validateDate};
