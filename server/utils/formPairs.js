/** This utility function generates and returns an array of supported currency pairs.
 * Currently the free API I use allows no more than 2 pairs per request, so this array consists of sub-arrays,
 * each containing straight and reversed combination of 2 currencies ( e.g. ['USD_CAD', 'CAD_USD'] )*/

const supportedCur = require('./../config/supportedCurrencies.json');

const formPairs = () => {
    const currencies = Object.values(supportedCur);
    let pairsArray = [];

    for (const element1 of currencies) {
        const temp = currencies.slice(currencies.indexOf(element1) + 1);
        for (const element2 of temp) {
            pairsArray.push([
                `${element1}_${element2}`,
                `${element2}_${element1}`
            ]);
        }

    }
    return pairsArray;
};


module.exports = {formPairs};