const axios = require('axios');
const moment = require('moment');

const mongoose = require('./../db/mongoose'); // path to config. won't work without it
const {LiveRecord} = require('../models/historicalRecord');
const {formPairs} = require('./formPairs');
const {liveExRatesReq} = require('./formLinks');

const wait = (delay, ...args) => new Promise(resolve => setTimeout(resolve, delay, ...args));



const saveLiveRatesAllCurrencies = async () => {
    try {
        const pairsArray = formPairs();

        for (const pairsPerReq of pairsArray) {

            const log = await saveLiveRatesOneReq(pairsPerReq);
            console.log(`Requesting ${pairsPerReq.join(' ')}: ${log}`); // logging success or failure for the requested pairs

            await wait(1000);
        }
        return 'All done';

    } catch (e) {
        return e.message;
    }
};

const saveLiveRatesOneReq = async (pairsPerReq) => {
    try {
        const response = await axios.get(liveExRatesReq(pairsPerReq));

        const timeUTC = moment.utc().startOf('hour');
        let log = [];

        for (const pair of pairsPerReq) {
            const record = {
                pair,
                date: timeUTC.format(),
                rate: response.data[pair]
            };

            try {
                await new LiveRecord(record).save();
                log.push(`Done!`);
            } catch (e) {
                log.push(`${pair} - ${e.message}!`);
            }
        }
        return log.join(' ');

    } catch (e) {
        return e.message;
    }
};

// TODO - find().count()

// TODO - Remove Unused on Cleanup

module.exports = {saveLiveRatesAllCurrencies, saveLiveRatesOneReq};
