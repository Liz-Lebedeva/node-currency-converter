const axios = require('axios');
const moment = require('moment');

const mongoose = require('./../db/mongoose'); // path to config. won't work without it
const {HistoricalRecord} = require('../models/historicalRecord');
const {formPairs} = require('./formPairs');
const {historicalOneDayReq, historicalRangeReq} = require('./formLinks');

const wait = (delay, ...args) => new Promise(resolve => setTimeout(resolve, delay, ...args));

const saveDataAllCurrenciesOneDay = async (date) => {
    try {
        const pairsArray = formPairs();

        for (const pairsPerReq of pairsArray) {

            const log = await saveDataOneReqOneDay(pairsPerReq, date);
            console.log(`Requesting ${pairsPerReq.join(' ')}: ${log}`); // logging success or failure for the requested pairs

            // CHANGE WAIT VALUE IF NECESSARY TO NOT EXCEED 100 REQ per HOUR
            await wait(1000);
        }
        return 'All done';

    } catch (e) {
        return e.message;
    }
};

const saveDataOneReqOneDay = async (pairsPerReq, date) => {
    try {
        const response = await axios.get(historicalOneDayReq(pairsPerReq, date));

        const dateUTC = moment.utc(date).hour(12);

        let log = [];

        for (const pair of pairsPerReq) {
            const record = {
                pair,
                date: dateUTC.format(),
                rate: response.data[pair][date]
            };

            try {
                await new HistoricalRecord(record).save();
                log.push(`Done!`);
            } catch (e) {
                log.push(`${pair} - ${e.message}!`);
            }
        }
        return log.join(' ');

    } catch (e) {
        return e.message;
    }
};

const saveDataAllCurrenciesLongRange = async (fromDate, toDate) => {
    try {
        const pairsArray = formPairs();

        for (const pairsPerReq of pairsArray) {
            console.log(`Requesting ${pairsPerReq.join(' ')}:`); // logging requested pairs

            const log = await saveDataOneReqLongRange(pairsPerReq, fromDate, toDate);

            console.log(log); // logging success or failure
            console.log('--------------------------');

            // CHANGE WAIT VALUE IF NECESSARY TO NOT EXCEED 100 REQ per HOUR
            await wait(300000);
        }
        return 'All done';

    } catch (e) {
        return e.message;
    }
};

const saveDataOneReqLongRange = async (pairsPerReq, fromDate, toDate) => {
    try {
        const from = moment(fromDate, 'YYYY-MM-DD');
        const to = moment(toDate, 'YYYY-MM-DD');
        const range = to.diff(from, 'days');

        if (range <= 0) {
            throw new Error('Invalid range of dates')
        } else if (range/8 > 100) {
            throw new Error('Range is too big while we are using free APIs')
        } else {
            let datesArray = [];
            let iterator = moment(from);  // cloned moment 'from'

            while (to.diff(iterator, 'days') > 8) {
                const dates = {
                    from: iterator.format('YYYY-MM-DD'),
                    to: iterator.add(7, 'days').format('YYYY-MM-DD')
                };
                datesArray.push(dates);
                iterator.add(1, 'days');
            }
            const dates = {
                from: iterator.format('YYYY-MM-DD'),
                to: to.format('YYYY-MM-DD')
            };
            datesArray.push(dates);

            let log = [];
            for (const dates of datesArray) {
                const res = await saveDataOneReqShortRange(pairsPerReq, dates.from, dates.to);
                log.push(res);
            }
            return log.join('\n');
        }

    } catch (e) {
        return e.message;
    }
};

const saveDataOneReqShortRange = async (pairsPerReq, fromDate, toDate) => {
    try {
        const response = await axios.get(historicalRangeReq(pairsPerReq, fromDate, toDate));

        let iterator = moment.utc(fromDate).hour(12);
        const to = moment.utc(toDate).hour(12);

        let log = [];

        while (to.diff(iterator, 'days') >= 0) {
            for (const pair of pairsPerReq) {
                const record = {
                    pair,
                    date: iterator.format(),
                    rate: response.data[pair][iterator.format('YYYY-MM-DD')]
                };

                try {
                    await new HistoricalRecord(record).save();
                    log.push(`${pair} - ${iterator.format('YYYY-MM-DD')} - Done!`);
                } catch (e) {
                    log.push(`${pair} - ${iterator.format('YYYY-MM-DD')} - ${e.message}`);
                }
            }
            iterator.add(1, 'days');
        }
        return log.join('\n');

    } catch (e) {
        return e.message;
    }
};


// TODO - Remove Unused on Cleanup

module.exports = {saveDataAllCurrenciesLongRange, saveDataAllCurrenciesOneDay, saveDataOneReqOneDay, saveDataOneReqShortRange, saveDataOneReqLongRange};
